package com.sprhib.controller;

import com.sprhib.model.Team;
import com.sprhib.service.TeamService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TeamControllerTest {

    TeamController teamController;
    TeamService teamService;

    @Before
    public void setUp(){
        teamService = mock(TeamService.class);
        teamController = new TeamController();
        teamController.teamService = teamService;
    }

   @Test
    public void testThatAddingItemWillSaveItToDbAndReturnsAMessage(){
        ModelAndView modelAndView = teamController.addingTeam(null);

       verify(teamService).addTeam(null);
       assertTrue(modelAndView.getModel().containsKey("message"));
       assertThat((String)modelAndView.getModel().get("message"), is("Team was successfully added."));
   }

    @Test
    public void testeditTeamPage(){
        Team myTeam = new Team();
        when(teamService.getTeam(5)).thenReturn(myTeam);
        ModelAndView modelAndView = teamController.editTeamPage(5);
        assertThat(modelAndView.getViewName(), is("edit-team-form"));
        assertThat((Team)modelAndView.getModel().get("team"), is(myTeam));

    }
}