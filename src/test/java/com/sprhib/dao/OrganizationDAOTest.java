package com.sprhib.dao;

import com.sprhib.init.CoreConfig;
import com.sprhib.model.Organization;
import com.sprhib.model.Team;
import com.sprhib.service.OrganizationService;
import com.sprhib.service.TeamService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@ContextConfiguration(classes = CoreConfig.class)
@RunWith(SpringJUnit4ClassRunner.class)
public class OrganizationDAOTest {

    @Autowired
    private TeamService teamService;

    @Autowired
    private OrganizationService organizationService;

    @Test
    public void addOrganizationWithTeams() throws Exception {

        Team team = new Team();
        team.setName("1");
        teamService.addTeam(team);

        Organization organization = new Organization();
        organization.setName("xxx");
        organization.addTeam(team);
        organizationService.addOrganization(organization);

        organization = new Organization();
        organization.setName("yyy");
        organization.addTeam(team);
        organizationService.addOrganization(organization);

        //TODO: 24-Sep-14 Andrei Sljusar: get organization and assert
    }
}