CREATE SEQUENCE hibernate_sequence INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 START 1 CACHE 1;

CREATE TABLE organization
(
  id integer NOT NULL,
  name varchar(255),
  CONSTRAINT organization_pkey PRIMARY KEY (id)
);

CREATE TABLE team
(
  id integer NOT NULL,
  name varchar (255),
  rating integer,
  CONSTRAINT team_pkey PRIMARY KEY (id)
);

CREATE TABLE organization_team
(
  organization_id integer NOT NULL,
  team_id integer NOT NULL,
  CONSTRAINT organization_team_pkey PRIMARY KEY (organization_id, team_id),
  CONSTRAINT organization_fkey FOREIGN KEY (organization_id) REFERENCES organization (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT team_fkey FOREIGN KEY (team_id) REFERENCES team (id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE teammember
(
id integer NOT NULL,
name varchar(255),
CONSTRAINT teammember_pkey PRIMARY  KEY (id)

);


CREATE TABLE team_teammember
(
team_id integer NOT NULL,
member_id integer NOT NULL,
 CONSTRAINT team_teammember_pkey PRIMARY  KEY (team_id,member_id),
 CONSTRAINT team_fkey FOREIGN KEY (team_id) REFERENCES team(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION,
 CONSTRAINT member_fkey FOREIGN KEY (member_id) REFERENCES  teammember(id) MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);