<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><spring:message code="message.listTeams"/></title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <span class="label label-success">${message}</span>
<h1><spring:message code="message.listMembers"/></h1>
<p><spring:message code="message.listMembersMessage"/></p>
<table border="1px" cellpadding="0" cellspacing="0" >
<thead>
<tr>
<th width="10%"><spring:message code="label.id"/></th><th width="15%"><spring:message code="label.name"/></th><th width="10%"><spring:message code="label.action"/></th>
</tr>
</thead>
<tbody>
<c:forEach var="member" items="${members}">
<tr>
	<td>${member.id}</td>
	<td>${member.name}</td>
	<td>
	<a href="${pageContext.request.contextPath}/member/edit/${team.id}.html"><spring:message code="label.edit"/></a><br/>
	<a href="${pageContext.request.contextPath}/member/delete/${team.id}.html"><spring:message code="label.delete"/></a><br/>
	</td>
</tr>
</c:forEach>
</tbody>
</table>
</div>
<script src="/resources/js/jquery-2.1.1.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>