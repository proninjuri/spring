<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="message.addTeadPage"/></title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <h1><spring:message code="message.addMemberPage"/></h1>

    <p><spring:message code="message.memberMessage"/></p>

    <h2>${message}</h2>
    <form:form method="POST" commandName="member" action="${pageContext.request.contextPath}/member/add.html">
        <table>
            <tbody>
            <tr>
                <td>
                    <spring:message code="label.name" var="nameVar"/>
                    <form:input type="text" class="form-control" placeholder="${nameVar}" path="name"/>
                </td>
            </tr>
            <tr>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <button type="submit" class="btn btn-default navbar-btn"><spring:message code="label.add"/></button>
                </td>
            </tr>
            </tbody>
        </table>
    </form:form>

    <%--<p><a href="${pageContext.request.contextPath}/index.html">Home page</a></p>--%>
</div>
<script src="/resources/js/jquery-2.1.1.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>