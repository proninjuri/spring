<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<?xml version="1.0" encoding="ISO-8859-1" ?>

<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="ISO-8859-1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title><spring:message code="message.editTeamPage"/></title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
<div class="container">
    <jsp:include page="header.jsp"/>
    <h1><spring:message code="message.editOrganizationPage"/></h1>

    <p><spring:message code="message.editOrganizationMessage"/></p>

    <p>${message}</p>
    <form:form method="POST" commandName="organization"
               action="${pageContext.request.contextPath}/organization/edit/${organization.id}.html">
        <table>
            <tbody>
            <tr>
                <td><spring:message code="label.name"/></td>
                <td><form:input path="name"/></td>
            </tr>
            <tr>
                <td><spring:message code="label.teams"/></td>
                <td>
                    <form:select path="teams" var="team">
                        <form:options  itemValue="${team.id}" itemLabel="${team.name}"/>
                    </form:select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Edit"/></td>
                <td></td>
            </tr>
            </tbody>
        </table>
    </form:form>

</div>
<script src="/resources/js/jquery-2.1.1.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>