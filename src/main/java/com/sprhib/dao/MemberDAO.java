package com.sprhib.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sprhib.model.Team;
import com.sprhib.model.TeamMember;

@Repository
public class MemberDAO {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public void addMember(TeamMember member) {
        getCurrentSession().save(member);
	}

	public void updateTeam(TeamMember member) {
		TeamMember memberToUpdate = getMember(member.getId());
        memberToUpdate.setId(member.getId());
		memberToUpdate.setName(member.getName());
		getCurrentSession().update(memberToUpdate);
		
	}

	public TeamMember getMember(int id) {
        return (TeamMember) getCurrentSession().get(TeamMember.class, id);
	}

	public void deleteTeam(int id) {
		TeamMember member = getMember(id);
		if (member != null)
			getCurrentSession().delete(member);
	}

	@SuppressWarnings("unchecked")
	public List<TeamMember> getMembers() {
		return getCurrentSession().createQuery("from TeamMember").list();
	}

}
