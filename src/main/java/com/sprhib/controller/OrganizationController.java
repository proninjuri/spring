package com.sprhib.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.sprhib.model.Organization;
import com.sprhib.service.OrganizationService;
import com.sprhib.service.TeamService;

@Controller
@RequestMapping(value="/organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;
    @Autowired
    private TeamService teamService;

    @RequestMapping(value="/add", method= RequestMethod.GET)
    public ModelAndView addOrganizationPage() {
        ModelAndView modelAndView = new ModelAndView("add-organization-form");
        modelAndView.addObject("organization", new Organization());
        modelAndView.addObject("teams", teamService.getTeams());

        return modelAndView;
    }

    @RequestMapping(value="/add", method=RequestMethod.POST)
    public ModelAndView addingOrganization(@ModelAttribute Organization organization) {
        ModelAndView modelAndView = new ModelAndView("list-of-organization");
        organizationService.addOrganization(organization);
        String message = "Organization was successfully added.";
        showModel(modelAndView, message);
        return modelAndView;
    }

    private void showModel(ModelAndView modelAndView, String message) {
        List<Organization> organizations = organizationService.getOrganizations();
        modelAndView.addObject("organizations",organizations);
        modelAndView.addObject("message", message);
    }

    @RequestMapping(value="/list")
    public ModelAndView listofOrganizations(){
        ModelAndView modelAndView = new ModelAndView("list-of-organization");
        List<Organization> organizations = organizationService.getOrganizations();
        modelAndView.addObject("organizations",organizations);
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.GET)
    public ModelAndView editTeamPage(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("edit-organization-form");
        Organization organization = organizationService.getOrganization(id);
        modelAndView.addObject("organization",organization);
        modelAndView.addObject("teams",teamService.getTeams());
        return modelAndView;
    }

    @RequestMapping(value="/edit/{id}", method=RequestMethod.POST)
    public ModelAndView edditingTeam(@ModelAttribute Organization organization, @PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("list-of-organization");
        organizationService.updateOrganization(organization);
        String message = "Team was successfully edited.";
        showModel(modelAndView,message);
        return modelAndView;
    }

    @RequestMapping(value="/delete/{id}", method=RequestMethod.GET)
    public ModelAndView deleteTeam(@PathVariable Integer id) {
        ModelAndView modelAndView = new ModelAndView("list-of-organization");
        organizationService.deleteTeam(id);
        String message = "Team was successfully deleted.";
        showModel(modelAndView,message);
        return modelAndView;
    }

}
