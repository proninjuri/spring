package com.sprhib.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;


@Entity
public class TeamMember {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    Set<Team> team;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Set<Team> getTeam() {
//        return team;
//    }
//
//    public void setTeam(Set<Team> team) {
//        this.team = team;
//    }
}
