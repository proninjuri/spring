package com.sprhib.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Team {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    private Integer rating;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name="team_teammember",inverseJoinColumns = {@JoinColumn(name="member_id")})
    private Set<TeamMember> teamMember = new HashSet<TeamMember>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Set<TeamMember> getTeamMember() {
        return teamMember;
    }

    public void setTeamMember(Set<TeamMember> teamMember) {
        this.teamMember = teamMember;
    }
}
