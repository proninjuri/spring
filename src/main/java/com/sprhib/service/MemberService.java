package com.sprhib.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.sprhib.dao.MemberDAO;
import com.sprhib.model.TeamMember;

@Service
@Transactional
public class MemberService {

    @Autowired private MemberDAO memberDAO;

    public void addMember(TeamMember member) {
        memberDAO.addMember(member);
    }

    public void updateMember(TeamMember member) {
        memberDAO.updateTeam(member);
    }

    public TeamMember getMember(int id) {
        return memberDAO.getMember(id);
    }

    public void deleteMember(int id) {
        memberDAO.deleteTeam(id);
    }

    public List<TeamMember> getMembers() {
        return memberDAO.getMembers();
    }

}
