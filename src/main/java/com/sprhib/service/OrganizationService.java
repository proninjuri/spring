package com.sprhib.service;

import com.sprhib.dao.OrganizationDAO;
import com.sprhib.model.Organization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class OrganizationService {

    @Autowired
    private OrganizationDAO organizationDAO;

    public void addOrganization(Organization organization) {
        organizationDAO.addOrganization(organization);
    }

    public List<Organization> getOrganizations() {
        return organizationDAO.getOrganizations();
    }

    public Organization getOrganization(Integer id) {
        return organizationDAO.getOrganization(id);
    }

    public void updateOrganization(Organization organization) {
        organizationDAO.updateOrganization(organization);
    }

    public void deleteTeam(Integer id) {
        organizationDAO.deleteOrganization(id);
    }
}
